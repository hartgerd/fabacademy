# 17. Machine design

## Embroidery machine

My part is to provide an Arduino Uno with a shield, printer drivers and software that the stepper motors for the work process can be controlled with a G code.

First of all we need the Arduino IDE again and have to load GRBL into the library. Then I put the shield on the Arduino and uploaded GBRL to the Arduino uno.

<img src="../../images/week17/1.jpg" width = 900px><br>
<img src="../../images/week17/2.jpg" width = 900px><br>
<img src="../../images/week17/3.jpg" width = 900px><br>
<img src="../../images/week17/4.jpg" width = 900px><br>


## Check the motors

The next step is to check and prepare the motors so far that they can be connected to the motor drivers on the shield.
<img src="../../images/week17/7.jpg" width = 900px><br>

A simple trick to check which pair of cables belongs together is to connect the cables one after the other and to check on the axis whether the resistance increases.
If the resistance increases noticeably, the right pair of cables has been found.

<img src="../../images/week17/5.jpg" width = 900px><br>

Now the corresponding pairs must be soldered to the plugs.

<img src="../../images/week17/6.jpg" width = 900px><br>

Another way to find the corresponding cable pairs, especially if there are more than four cables, is to measure the resistance. The cable pairs with the greatest resistance belong together.

<img src="../../images/week17/8.jpg" width = 900px><br>


## Interface and programm for the gcode

In order to check whether the GRBL is working, we need an interface for CNC milling.
Here we chose CNCjs.

<img src="../../images/week17/9.jpg" width = 900px><br>


##Testing the motors, arduino, shield and CNCjs 1.9.22

<iframe width="560" height="315" src="https://www.youtube.com/embed/qn0MD3WlKm4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Since we want to use a stronger motor to lift the sewing facial expressions, we decided on a more powerful printer driver for the Z-axis.

Here we chose the satstep6600 from Daniele Ingrassia.

<img src="../../images/week17/10.jpg" width = 900px><br>

<img src="../../images/week17/18.jpg" width = 900px><br>


## First  setting with satstep6600 and a bigger motor

<img src="../../images/week17/11.jpg" width = 900px><br>


## Check the new motor driver and the motors

<iframe width="560" height="315" src="https://www.youtube.com/embed/_Rigk8XMwBM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


##INK STITCH

For Inkscape you can download a plugin called INK STITCH so that we can also send embroidery files as gcode to the machine.

<img src="../../images/week17/12.jpg" width = 900px><br>

<img src="../../images/week17/13.jpg" width = 900px><br>

For the german version.

<img src="../../images/week17/14.jpg" width = 900px><br>

Unzip the ZIP file in the folder.

<img src="../../images/week17/15.jpg" width = 900px><br>

To do this, paste the target directory copied above.

<img src="../../images/week17/16.jpg" width = 900px><br>

Restart Inkscape

<img src="../../images/week17/17.jpg" width = 900px><br>

Inscape is installed and it is possible to load directly extensions.


## Continue with the electronics

Put in the jumper

<img src="../../images/week17/19.jpg" width = 900px><br>

<img src="../../images/week17/44.jpg" width = 900px><br>

- [Pololu](https://www.pololu.com/product/1182)

Clear the situation

<img src="../../images/week17/24.jpg" width = 900px><br>

<img src="../../images/week17/27.jpg" width = 900px><br>

First settings of the electronics at the machine

<img src="../../images/week17/22.jpg" width = 900px><br>

Top view

<img src="../../images/week17/23.jpg" width = 900px><br>



## Calibrate the axis/motors

<img src="../../images/week17/25.jpg" width = 900px><br>

<iframe width="1903" height="768" src="https://www.youtube.com/embed/cU6PMQsNqIc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<img src="../../images/week17/26.jpg" width = 900px><br>

Change the settings

<img src="../../images/week17/42.jpg" width = 900px><br>

<img src="../../images/week17/43.jpg" width = 900px><br>

- [Grbl v1.1 Configuration](https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration#26---homing-debounce-milliseconds)


## Fit and rework the 3d printed parts for the electronics

<img src="../../images/week17/30.jpg" width = 900px><br>

<img src="../../images/week17/33.jpg" width = 900px><br>

<img src="../../images/week17/31.jpg" width = 900px><br>

<img src="../../images/week17/35.jpg" width = 900px><br>

<img src="../../images/week17/36.jpg" width = 900px><br>


## Calibrate motordriveres with the potentiometer

<iframe width="1903" height="768" src="https://www.youtube.com/embed/LZmv_jjYj2g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<img src="../../images/week17/28.jpg" width = 900px><br>

<iframe width="1903" height="768" src="https://www.youtube.com/embed/protqbBKLLo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## First assembly test x and y axis

<img src="../../images/week17/34.jpg" width = 900px><br>

<iframe width="1332" height="566" src="https://www.youtube.com/embed/eVLEhvgH014" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## z axis

First settings

<img src="../../images/week17/37.jpg" width = 900px><br>

Shorten the chain

<img src="../../images/week17/38.jpg" width = 900px><br>


## Testing the movements with a paper handkerchief

<img src="../../images/week17/39.jpg" width = 900px><br>


## No test
<img src="../../images/week17/40.jpg" width = 900px><br>

<iframe width="1332" height="517" src="https://www.youtube.com/embed/D8RCPV6qywg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Motiv and gcode

<img src="../../images/week17/41.jpg" width = 900px><br>

## How to make the gcode

<iframe width="1903" height="719" src="https://www.youtube.com/embed/KvF-DwdGb94" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Useful links
- [Pololu](https://www.pololu.com/product/1182)
- [satstep6600](https://github.com/satstep/satstep6600)
- [Grbl v1.1 Configuration](https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration#26---homing-debounce-milliseconds)
- [inkstitch.org](https://inkstitch.org/de/)
- [github.com/inkstitch](https://github.com/inkstitch/inkstitch/releases/tag/v1.28.0)
