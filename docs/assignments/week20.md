# 20. Construktion Side Router


##The Board
I want to control my milling machine with grbl and
decided to adapt the Satchakit-grbl from Daniele Ingrassia to my needs.

<img src="../../images/week20/3.jpg" width = 900px><br>

ich habe das board so verändert, das ich alles für mich überflüssige weggelassen habe und alle wichtigen anschlüsse nach unten und nach rechts gelegt habe.
Dazu habe ich zum ersten mal mit vias gearbeitet. vias geben einem die möglichkeit auch die Rückseite des boards zu bespielen.

<img src="../../images/week20/4.jpg" width = 900px><br>

Dazu habe ich in eagle den Schaltplan verändert und im anschluss die Platine neu gestaltet.

<img src="../../images/week20/5.jpg" width = 900px><br>

mit gimp habe ich die heruntergeladenen bilddateien dann bearbeitet um für die traces , den cutout und die holes die entsprechenden bildvorlagen zu bekommen um sie im anschluss bei fabmodules hochladen zu können.
hier konnten ich mir dann auch die drei entsprechenden g-codes für die Roland Mill mdx-40 herunterladen
und das board fräsen. den prozess habe ich im assignment elektronik produktion schon eingehender dokumentiert.

<img src="../../images/week20/70.jpg" width = 900px><br>
<img src="../../images/week20/6.jpg" width = 900px><br>
<img src="../../images/week20/71.jpg" width = 900px><br>

nach dem fräsen der platine habe ich mit einer kupferbeschichteten klebefläche die fräsung auf der rückseite simuliert. da ich die anschüsse pulse z-Y-X gerne nach unten haben wollte, habe ich sie auf die rückseite gelegt und von oben nach unten später  mit pinheadern verbunden.

<img src="../../images/week20/60.jpg" width = 900px><br>

nun habe ich die bauteile auf der platine verlötet
<img src="../../images/week20/61.jpg" width = 900px><br>

hier sieht man sehr gut wie ich die ober und die unterseite mit den pinheadern verbunden habe.
<img src="../../images/week20/62.jpg" width = 900px><br>

gemacht
<img src="../../images/week20/63.jpg" width = 900px><br>



###Problems
Kurzschluß:                                                                                                          
Ich hatte in fab modules die einstellungen wohl etwas ungünstig gewält. im nachhinein habe ich gesehen, dass in der vorschau die fehler schon ersichtlich waren. an den gekennzeichneten stellen hat der fräser nicht durchgefräst.

<img src="../../images/week20/202.jpg" width = 900px><br>

da ich alles überflüssige weglassen wollte, habe ich sck und miso leider auch entsorgt.
ich musste sie nachträglich wieder anbringen. Dazu habe ich ein kleines loch mit dem Proxxon direkt an den pinnen vom microprozessor gebohrt und diese ebenfalls nach unten gelegt.
nun hatte ich alle anschlüsse um den bootloader zu brennen.

<img src="../../images/week20/203.jpg" width = 900px><br>

###Program the Board
Beim Programmieren hat mir die Anleitung vom Satchakit-GRBL geholfen.

<img src="../../images/week20/48.jpg" width = 900px><br>

Ich habe zunächst alles einmal angeschlossen und überprüft ob alles mit strom versorgt wird
<img src="../../images/week20/49.jpg" width = 900px><br>

zum Hochladen der Arduino isp habe ich das neue board vom strom getrennt indem ich gnd und vcc abgeklemmt habe.
<img src="../../images/week20/50.jpg" width = 900px><br>

Nach dem hochladen der arduino isp habe ich den bootloader auf das neue board hochgeladen
<img src="../../images/week20/51.jpg" width = 900px><br>

###load up GRBL
Im anschluss habe ich Grbl aufgespielt
<img src="../../images/week20/52.jpg" width = 900px><br>

nun ist das board einsatzbereit!
<img src="../../images/week20/53.jpg" width = 900px><br>



- [Fab Modules](http://fabmodules.org/)
- [satchakit-grbl](https://github.com/satshakit/satshakit-grbl)
- [limit-switches](https://github.com/gnea/grbl/wiki/Wiring-Limit-Switches)



##Prepare the motors
<img src="../../images/week20/201.jpg" width = 900px><br>


<img src="../../images/week20/7.jpg" width = 900px><br>
- [limit-switches](https://github.com/gnea/grbl/wiki/Wiring-Limit-Switches)s



##Testing motordrivers, motors and endstop with satshakit, GRBL and cncjs 1.9.22

<iframe width="1093" height="480" src="https://www.youtube.com/embed/s1beLYL_sus" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



Endstops in am Abschlussboard in GRBL umstellen
<img src="../../images/week20/300.jpg" width = 900px><br>


## y-z-Axis Construktion Prozess
<img src="../../images/week20/303.jpg" width = 900px><br>
Bought by aliexpress
<img src="../../images/week20/304.jpg" width = 900px><br>
<img src="../../images/week20/305.jpg" width = 900px><br>
<img src="../../images/week20/306.jpg" width = 900px><br>
<img src="../../images/week20/307.jpg" width = 900px><br>
<img src="../../images/week20/308.jpg" width = 900px><br>
Auch bei openbuilds zu bekommen.
<img src="../../images/week20/309.jpg" width = 900px><br>
Download der STEP Dateien
<img src="../../images/week20/310.jpg" width = 900px><br>
<img src="../../images/week20/311.jpg" width = 900px><br>
First look in MOI
<img src="../../images/week20/312.jpg" width = 900px><br>
Die Frässpindel
<img src="../../images/week20/313.jpg" width = 900px><br>
Anpassen der GRABCAD Dateien auf meine Bedürfnisse in Fusion 360
<img src="../../images/week20/314.jpg" width = 900px><br>
<img src="../../images/week20/315.jpg" width = 900px><br>
<img src="../../images/week20/316.jpg" width = 900px><br>
<img src="../../images/week20/317.jpg" width = 900px><br>
<img src="../../images/week20/318.jpg" width = 900px><br>
<img src="../../images/week20/319.jpg" width = 900px><br>
<img src="../../images/week20/320.jpg" width = 900px><br>
<img src="../../images/week20/325.jpg" width = 900px><br>
<img src="../../images/week20/326.jpg" width = 900px><br>

<img src="../../images/week20/329.jpg" width = 900px><br>
<img src="../../images/week20/330.jpg" width = 900px><br>
<img src="../../images/week20/331.jpg" width = 900px><br>
<img src="../../images/week20/332.jpg" width = 900px><br>
<img src="../../images/week20/333.jpg" width = 900px><br>
<img src="../../images/week20/334.jpg" width = 900px><br>
<img src="../../images/week20/335.jpg" width = 900px><br>
<img src="../../images/week20/336.jpg" width = 900px><br>
<img src="../../images/week20/337.jpg" width = 900px><br>
<img src="../../images/week20/338.jpg" width = 900px><br>

## x-Axis

<img src="../../images/week20/301.jpg" width = 900px><br>
<img src="../../images/week20/302.jpg" width = 900px><br>

<img src="../../
images/week20/321.jpg" width = 900px><br>

<img src="../../images/week20/327.jpg" width = 900px><br>
<img src="../../images/week20/328.jpg" width = 900px><br>

<img src="../../images/week20/322.jpg" width = 900px><br>
<img src="../../images/week20/323.jpg" width = 900px><br>
<img src="../../images/week20/324.jpg" width = 900px><br>



## Zuschnitt
<img src="../../images/week20/400.jpg" width = 900px><br>
<img src="../../images/week20/401.jpg" width = 900px><br>

##License
##License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz</a>.
