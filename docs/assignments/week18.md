# 18. Wildcard week

##More chocolate

<img src="../../images/week18/1.jpg" width = 900px><br>
<img src="../../images/week18/30.jpg" width = 900px><br>
<img src="../../images/week18/31.jpg" width = 900px><br>
<img src="../../images/week18/32.jpg" width = 900px><br>
<img src="../../images/week18/33.jpg" width = 900px><br>
<img src="../../images/week18/34.jpg" width = 900px><br>
<img src="../../images/week18/35.jpg" width = 900px><br>
<img src="../../images/week18/36.jpg" width = 900px><br>
<img src="../../images/week18/37.jpg" width = 900px><br>



<img src="../../images/week18/38.jpg" width = 900px><br>
<img src="../../images/week18/40.jpg" width = 900px><br>
<img src="../../images/week18/2.jpg" width = 900px><br>
<img src="../../images/week18/3.jpg" width = 900px><br>
<img src="../../images/week18/4.jpg" width = 900px><br>
<img src="../../images/week18/5.jpg" width = 900px><br>
<img src="../../images/week18/6.jpg" width = 900px><br>
<img src="../../images/week18/7.jpg" width = 900px><br>
<img src="../../images/week18/8.jpg" width = 900px><br>
<img src="../../images/week18/9.jpg" width = 900px><br>


##Second Try

<img src="../../images/week18/48.jpg" width = 900px><br>
<img src="../../images/week18/49.jpg" width = 900px><br>
<img src="../../images/week18/50.jpg" width = 900px><br>
<img src="../../images/week18/51.jpg" width = 900px><br>
<img src="../../images/week18/52.jpg" width = 900px><br>


<img src="../../images/week18/60.jpg" width = 900px><br>
<img src="../../images/week18/61.jpg" width = 900px><br>


<img src="../../images/week18/62.jpg" width = 900px><br>
<img src="../../images/week18/63.jpg" width = 900px><br>
<img src="../../images/week18/64.jpg" width = 900px><br>

<iframe width="1269" height="483" src="https://www.youtube.com/embed/uYDwE4pt_Lk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>








<iframe width="2026" height="795" src="https://www.youtube.com/embed/UsDnkrDvkBo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Research

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

> "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

## Useful links

- [Jekyll](http://jekyll.org)
- [Google](http://google.com)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)

## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

## Gallery

![](../images/sample-photo.jpg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/jjNgJFemlC4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
