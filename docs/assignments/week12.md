# 12. Output devices

##AnalogInOutSerial

I would like to design a  building site milling machine as the final project. In preparation, I would like to control a spindle and regulate the speed using a potentiometer.
 Since my knowledge of C ++ is slowly growing, I will first combine different finished solutions on the Arduino to check whether everything works and then transfer this solution to my board from the Electronics Design Week.

<img src="../../images/week12/arduinobsp1.jpg" width = 900px><br>
<img src="../../images/week12/beispiel arduino analog.jpg" width = 900px><br>
```
/*
  Analog input, analog output, serial output

  Reads an analog input pin, maps the result to a range from 0 to 255 and uses
  the result to set the pulse width modulation (PWM) of an output pin.
  Also prints the results to the Serial Monitor.

  The circuit:
  - potentiometer connected to analog pin 0.
    Center pin of the potentiometer goes to the analog pin.
    side pins of the potentiometer go to +5V and ground
  - LED connected from digital pin 9 to ground

  created 29 Dec. 2008
  modified 9 Apr 2012
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/AnalogInOutSerial
*/

// These constants won't change. They're used to give names to the pins used:
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
const int analogOutPin = 9; // Analog output pin that the LED is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
}

void loop() {
  // read the analog in value:
  sensorValue = analogRead(analogInPin);
  // map it to the range of the analog out:
  outputValue = map(sensorValue, 0, 1023, 0, 255);
  // change the analog out value:
  analogWrite(analogOutPin, outputValue);

  // print the results to the Serial Monitor:
  Serial.print("sensor = ");
  Serial.print(sensorValue);
  Serial.print("\t output = ");
  Serial.println(outputValue);

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);
}
```


##Subject with variation


<img src="../../images/week12/arduino2.jpg" width = 900px><br>

I supplement the original setup with a 9V circuit for the motor and replace the LED with a Mosfet. I now put pin 9 with a 2.2K resistor at the base to pass on the PWM to the motor.

<iframe width="560" height="315" src="https://www.youtube.com/embed/3yVwNkvJF4U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


##Transfer this setting on my board


For the pin arrangement, I used the following example from the fabacademy archive as a guide.

<img src="../../images/week12/pin1.jpg" width = 900px><br>
http://archive.fabacademy.org/2018/labs/fablabdassault/students/oyedotun-ajweole/exercise10.html



###what pins on my board

I changed the pin assignment as follows.

<img src="../../images/week12/pin22.jpg" width = 900px><br>

<img src="../../images/week12/pin3.jpg" width = 900px><br>

Since it has happened several times that I had to count on 12 unknowns when troubleshooting, the process was easier because I transferred everything and only 3-4 unknowns came into question if something did not work.

<iframe width="560" height="315" src="https://www.youtube.com/embed/3Bk2MUbrTi4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
