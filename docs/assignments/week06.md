# 6. 3D Scanning and printing

##Printing

<img src="../../images/week06/1.jpg" width = 900px><br>

img src="../../images/week06/01.jpg" width = 900px><br>

<img src="../../images/week06/2.jpg" width = 900px><br>

<img src="../../images/week06/3.jpg" width = 900px><br>

<img src="../../images/week06/4.jpg" width = 900px><br>

<img src="../../images/week06/5.jpg" width = 900px><br>

<img src="../../images/week06/6.jpg" width = 900px><br>

<img src="../../images/week06/7.jpg" width = 900px><br>

<img src="../../images/week06/8.jpg" width = 900px><br>

<img src="../../images/week06/9.jpg" width = 900px><br>

<img src="../../images/week06/10.jpg" width = 900px><br>

<img src="../../images/week06/11.jpg" width = 900px><br>

<img src="../../images/week06/12.jpg" width = 900px><br>

<img src="../../images/week06/13.jpg" width = 900px><br>

<img src="../../images/week06/14.jpg" width = 900px><br>

<img src="../../images/week06/15.jpg" width = 900px><br>

<img src="../../images/week06/16.jpg" width = 900px><br>

<img src="../../images/week06/17.jpg" width = 900px><br>

<img src="../../images/week06/18.jpg" width = 900px><br>

<img src="../../images/week06/19.jpg" width = 900px><br>

<img src="../../images/week06/20.jpg" width = 900px><br>

<img src="../../images/week06/14.jpg" width = 900px><br>

<img src="../../images/week06/15.jpg" width = 900px><br>



### Useful links

- [voronator.com](https://www.voronator.com/)



##Scanning

###SHINING 3D PRO+
####Calibration and first tests

<img src="../../images/week06/30.jpg" width = 900px><br>

<img src="../../images/week06/31.jpg" width = 900px><br>

<img src="../../images/week06/32.jpg" width = 900px><br>

<img src="../../images/week06/33.jpg" width = 900px><br>

<img src="../../images/week06/34.jpg" width = 900px><br>

<img src="../../images/week06/35.jpg" width = 900px><br>

<img src="../../images/week06/36.jpg" width = 900px><br>

<img src="../../images/week06/37.jpg" width = 900px><br>

<img src="../../images/week06/38.jpg" width = 900px><br>

<img src="../../images/week06/39.jpg" width = 900px><br>

<img src="../../images/week06/40.jpg" width = 900px><br>

<img src="../../images/week06/50.jpg" width = 900px><br>

<img src="../../images/week06/51.jpg" width = 900px><br>

<img src="../../images/week06/52.jpg" width = 900px><br>

<img src="../../images/week06/53.jpg" width = 900px><br>

<img src="../../images/week06/54.jpg" width = 900px><br>

<iframe width="1013" height="480" src="https://www.youtube.com/embed/7_FRvk3Sbwk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<img src="../../images/week06/55.jpg" width = 900px><br>

<img src="../../images/week06/56.jpg" width = 900px><br>

<img src="../../images/week06/57.jpg" width = 900px><br>

<img src="../../images/week06/58.jpg" width = 900px><br>

<img src="../../images/week06/59.jpg" width = 900px><br>

<img src="../../images/week06/60.jpg" width = 900px><br>

<img src="../../images/week06/61.jpg" width = 900px><br>

<img src="../../images/week06/62.jpg" width = 900px><br>

<img src="../../images/week06/63.jpg" width = 900px><br>



- [voronator.com](https://www.voronator.com/)



###Scanning Isense

Da der I Sense in Verbindung mit einem Ipad im moment nicht funktioniert, nehme ich den Scan den wir im Unterricht im FabLab der Hochschule Rhein-Waal mit den Kindern zusammen angefertigt haben.
Dort entstanden auch die Aufnahmen die uns, Marc Kohlen die Schüler und mich mit unserer Arbeitsgemeinschaft "Die Digitale Manufaktur" zeigen.

<img src="../../images/week06/70.jpg" width = 900px><br>

<iframe width="1688" height="634" src="https://www.youtube.com/embed/LxHe5BDWFf0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
