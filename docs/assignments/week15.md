# 15. Molding and Casting



## Shitycornschokoladentaler

<img src="../../images/week15/1.jpg" width = 900px><br>
<img src="../../images/week15/2.jpg" width = 900px><br>
<img src="../../images/week15/3.jpg" width = 900px><br>

##Problems
Es wäre gut gewesen, wenn ich die Stärke des dünnsten Bohrers als Freiraum zwischen den Inseln gelassen hätte.
Der Bohrer kann so keine genauen Konturen zeichnen.

## New try

<img src="../../images/week15/4.jpg" width = 900px><br>
Um für den Bohrer dieses mal genug Platz einzuplanen habe ich mir einen Kreis (hellblau) mit einem Durchmesser von 0.8mm als bewegliche Schablone eingezeichnet.

<iframe width="2537" height="1080" src="https://www.youtube.com/embed/cgwM0XSFrCM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<img src="../../images/week15/23.jpg" width = 900px><br>

## Wax milling

<img src="../../images/week15/7.jpg" width = 900px><br>
<img src="../../images/week15/8.jpg" width = 900px><br>
<img src="../../images/week15/6.jpg" width = 900px><br>
<img src="../../images/week15/9.jpg" width = 900px><br>
<img src="../../images/week15/11.jpg" width = 900px><br>
<img src="../../images/week15/10.jpg" width = 900px><br>

<img src="../../images/week15/12.jpg" width = 900px><br>
<img src="../../images/week15/13.jpg" width = 900px><br>

##Silicon

<img src="../../images/week15/14.jpg" width = 900px><br>
<img src="../../images/week15/15.jpg" width = 900px><br>
<img src="../../images/week15/16.jpg" width = 900px><br>
<img src="../../images/week15/17.jpg" width = 900px><br>
<img src="../../images/week15/18.jpg" width = 900px><br>
<img src="../../images/week15/19.jpg" width = 900px><br>
<img src="../../images/week15/20.jpg" width = 900px><br>

<iframe width="1779" height="559" src="https://www.youtube.com/embed/qD2CebCaQhI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<img src="../../images/week15/21.jpg" width = 900px><br>

##Work with Schokolade
<img src="../../images/week15/22.jpg" width = 900px><br>







##How to work with Schokolade

<iframe width="2026" height="795" src="https://www.youtube.com/embed/--KcoWb8ZD4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<iframe width="2026" height="795" src="https://www.youtube.com/embed/UsDnkrDvkBo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



- [Google](http://google.com)
