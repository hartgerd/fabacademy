# 11. Input devices



## Sonarboard


<img src="../../images/week11/16.jpg" width = 900px><br>

<img src="../../images/week11/17.jpg" width = 900px><br>

<img src="../../images/week11/18.jpg" width = 900px><br>

<img src="../../images/week11/13.jpg" width = 900px><br>

<img src="../../images/week11/15.jpg" width = 900px><br>

<img src="../../images/week11/19.jpg" width = 900px><br>


In order to be able to read the Attiny 44A SSU with the serial monitor, I have to reprogram the board via the FTDI cable connection and SCK and MISO to RX and TX.
The serial monitor should help me to set the LEDs like this. That they glow green in the specified functional area and glow red in the areas where it is no longer optimal.
So pins 8 and 9 must serve as RX and TX.

<img src="../../images/week11/12.jpg" width = 900px><br>


<iframe width="1519" height="505" src="https://www.youtube.com/embed/i36Y7sKcYeU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Code Example

Use the three backticks to separate code.

```
#include <SoftwareSerial.h>

SoftwareSerial mySerial(4, 5); // RX, TX
int ledPinrot=0;
int ledPingruen=1;
int trigger=7;                               
int echo=8;                                  
long dauer=0;                                                                        
long entfernung=0;                         

void setup()
{

    mySerial.begin(9600);
    mySerial.println("start");
    pinMode(trigger, OUTPUT);               
    pinMode(echo, INPUT);

}

void loop()
{

    digitalWrite(trigger, LOW);              

    delay(5);                             
    digitalWrite(trigger, HIGH);             

    delay(10);                               
    digitalWrite(trigger, LOW);              

    dauer = pulseIn(echo, HIGH);             

    entfernung = (dauer/2) / 29.1;           


    if (entfernung >= 500 || entfernung <= 0)
{
    mySerial.println("no reading possible RED LED");
    digitalWrite(ledPinrot, HIGH);   
 delay(1000);
}
else
{
    mySerial.print(entfernung);
    mySerial.println(" cm");
    digitalWrite(ledPinrot, LOW);
    digitalWrite(ledPingruen, HIGH);

}
delay(1000);
}

...
