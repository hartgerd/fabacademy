# 10. Applications and implications

## How do I create a construction site router?

![](../images/week10/mht669D(1).jpg)

## What do I want to design?

I want to build a CNC milling machine where you can mill down all the plates from a pallet stack from top to bottom without having to put them somewhere else.
The pallets with the wood-based panels (usually 2500 x 1250) are delivered and I have the option of placing my machine directly on them and cutting the panels to size.
The machine is removed from the milled plate and the milled workpieces are taken down.
Leftover chips and dust are removed from the next plate and the machine is placed on it again so that the next milling process can begin.


## Why do I want to design something like that?
### Because i want to have it!

I have always enjoyed furniture and product development and am interested in FAB houses and tiny houses. In the past few years with the insight into the FABLAB I have learned that the means of production, at least if you are ready to assemble or design them yourself, have become very inexpensive. Here I have often looked for machines that are tailored to me and my needs. For a FAB house, products for urban gardening or for outdoor furniture. Since I have a large garden but do not want to rent a hall to be productive, I would like to have a stack of slabs come in and mill some products off the stack. I could cover the stack with foil and put the router in the evening.
A good weather cutter, so to speak.
I could also imagine that I would place the router in the building materials trade directly on the stack and mill the things I need there and save myself the delivery of the bulky, large panels.

## Who's done what beforehand?
### Outdoormill/ Rough-Trade-Mill/ Baustellenfräse/ Portabel Mill/ food for thought………..
- [printbot-crowlbot](https://makezine.com/2015/09/24/printrbot-crawlbot-review/)
- [fabacademy.org/archives/2015/eu/students/bassi.enrico](https://fabacademy.org/archives/2015/eu/students/bassi.enrico/project01.html)
- [fabacademy.org/archives/2015/eu/students/pugliese.gianluca](http://fabacademy.org/archives/2015/eu/students/pugliese.gianluca/project08.html)
- [maslowcnc](https://www.maslowcnc.com/)
- [mpcnc](https://docs.v1engineering.com/mpcnc/)
- [lowrider-cnc](https://www.v1engineering.com/lowrider-cnc/)
- [goliathcnc](https://www.goliathcnc.com/)
- [shaper](https://www.shapertools.com)
- [Google](http://google.com)
- [Google](http://google.com)
- [Google](http://google.com)

## First  CNC constructions

<img src="../../images/week10/1cnc.jpg" width = 900px><br>

The router is placed on a fixed support (green) at the rear end and adjusted on the wheels. The adjustment is used on the one hand to position the router so that it moves straight (parallel to the edge), and on the other hand it is used depending on the size of the plates to be processed (different colored areas) to enable the adjustment of the second milling so that the grooves are accurate fit together. In addition, the milling can be carried out directly up to the outer edges of the Y axes.

<img src="../../images/week10/2cnc.jpg" width = 900px><br>

<img src="../../images/week10/3cnc.jpg" width = 900px><br>

<img src="../../images/week10/4cnc.jpg" width = 900px><br>

The miller can extend beyond the outer edge of the plate material due to the protrusion of the X axis.
