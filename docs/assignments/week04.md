# 4. Computer controlled cutting

I wanted to build a geodesic dome. To do this, I have selected a construction on the following website and modified it according to my needs.

- [acidome.ru](http://acidome.ru/lab/calc/#1/2_Piped_D60_2V_R1.5_beams_120x30)

<img src="../../images/week04/10.jpg" width = 900px><br>

<img src="../../images/week04/11.jpg" width = 900px><br>

<img src="../../images/week04/12.jpg" width = 900px><br>



## Construction
First I constructed the five and six corner pillars. I adapted this to the diameter that the geodesic dome constructor calculated for my example.

<img src="../../images/week04/30.jpg" width = 900px><br>

Afterwards I inserted a low circular column with the same distance to the ends. I tried to estimate how far an interlocking of 3mm MDF brings stability.

<img src="../../images/week04/31.jpg" width = 900px><br>

I set the parameters in order to be able to quickly adjust all dimensions in the event of dimensional changes. The most important parameter for me was the material thickness.
I wanted to derive the connectors from these shapes later.

<img src="../../images/week04/32.jpg" width = 900px><br>

Next, I drew the bars with the angled faces on the ends that I wanted to dock onto the pentagonal and hexagonal columns. The circle should be intersected with.

<img src="../../images/week04/33.jpg" width = 900px><br>

Here, too, the most important parameter for me was the material thickness.

<img src="../../images/week04/34.jpg" width = 900px><br>

Then I connected the parts together so that they overlap.
Here I made sure that I put together both a connector with five rods and a connector with 6 rods.
Now I was able to pull off the bars from the original shape.

<img src="../../images/week04/35.jpg" width = 900px><br>

Next I added a smaller circle to the shape at the same height as the cutouts.

<img src="../../images/week04/36.jpg" width = 900px><br>

Now I was able to pull off the smaller circle at the right height and at the right angle in the material of the bars.

<img src="../../images/week04/37.jpg" width = 900px><br>

<img src="../../images/week04/38.jpg" width = 900px><br>

To check whether my strategy works, I put half the dome together so that it was clear that it would work.

<img src="../../images/week04/39.jpg" width = 900px><br>

I could now set exactly the material thickness that I need so that the parts hold together well with a little grip after they have been put together.

<img src="../../images/week04/40.jpg" width = 900px><br>

Now I still needed the DXF file to be able to continue working.

<img src="../../images/week04/41.jpg" width = 900px><br>

After saving the DXF files, I multiplied the parts with the MOI program and then opened them in Rhino. The data then went from Rhino to the laser cutter.

<img src="../../images/week04/42.jpg" width = 900px><br>



##Lasern

I imported the file into Rhino for laser cutting.
<img src="../../images/week04/1.jpg" width = 900px><br>

I could make the required settings under properties.
<img src="../../images/week04/2.jpg" width = 900px><br>

The following window opened.
I only needed Vector because I just wanted to cut.
The following attitudes have worked best for me.
<img src="../../images/week04/3.jpg" width = 900px><br>
<img src="../../images/week04/4.jpg" width = 900px><br>

With Set I was able to set which one on the working surface of the laser should start with. To do this, move the white field as far as you need it.
<img src="../../images/week04/5.jpg" width = 900px><br>

<img src="../../images/week04/6.jpg" width = 900px><br>
I chose the upper left corner.

<img src="../../images/week04/7.jpg" width = 900px><br>

<img src="../../images/week04/8.jpg" width = 900px><br>

##Print - the job goes to the machine
The first thing to do is to set the focus. This happens after a molded part, which is hooked in and is easy to adjust to the height of the workpiece.
<img src="../../images/week04/50.jpg" width = 900px><br>

Next, set Jog. To do this, it makes sense to switch on the small laser pointer, which shows you the exact starting point at which the cut will be made.
<img src="../../images/week04/51.jpg" width = 900px><br>


Before you start cutting, you have to switch on the suction so that the toxic fumes do not get into the work area and are instead filtered and directed outside.
<img src="../../images/week04/52.jpg" width = 900px><br>


<img src="../../images/week04/53.jpg" width = 900px><br>

Im Anschluß konnte ich den Job starten.
<img src="../../images/week04/54.jpg" width = 900px><br>


The chosen settings worked well.
<img src="../../images/week04/55.jpg" width = 900px><br>


The connections also had the necessary stability.
<img src="../../images/week04/56.jpg" width = 900px><br>

In the meantime I had thought about cutting out holes in the middle in order to be able to attach something later. For example, a paper that is cut to the shape to use the dome as a lamp.
To make the assembly a little easier, I made small notches at the larger angles on the hexagonal connector.
<img src="../../images/week04/57.jpg" width = 900px><br>

##Assembling

<img src="../../images/week04/60.jpg" width = 900px><br>
<img src="../../images/week04/61.jpg" width = 900px><br>
<img src="../../images/week04/62.jpg" width = 900px><br>
<img src="../../images/week04/63.jpg" width = 900px><br>

## Cutting Plotter

#SVG
<img src="../../images/week04/100.jpg" width = 900px><br>
<img src="../../images/week04/101.jpg" width = 900px><br>
#PNG
<img src="../../images/week04/102.jpg" width = 900px><br>

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz</a>.
