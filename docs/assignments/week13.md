# 13. Invention, intellectual property, and income

## Considerations/Plan

Since in this process I naturally learn from all the people who have already shared their work, I would like to make my work accessible to others and give them the opportunity to vary and develop it further.
At the moment I am still at the beginning of my work and I do not yet know where it will take me. Since I mostly work experimentally and process-oriented, it is difficult for me at the moment to also release the rights for commercial use.

###License

The license that comes closest to my ideas at the moment is the CC BY-NC-SA 4.0.
She shares the knowledge, gives everyone the opportunity to make the best of it for themselves and holds the opportunity for me to continue working on the project without having relinquished the commercial rights. However, it also offers the option of releasing these rights at a later date.

###Distribution

I'm not sure about the way I want to publish the work. Publication is guaranteed with participation in the fabacademy.

Another obvious step is publication on gitlab or github. I could also imagine developing a kit. Here I could also do workshops e.g. imagine happening with schools or an involvement in class.

Another way could be to create products with the milling machine and to publish them again in a process-oriented manner.
I think that the range of those interested is increased both in terms of teaching and product and project orientation.


## Plakat

<img src="../../images/week13/plakat 2.jpg" width = 900px><br>

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz</a>.
