# 14. Networking and communications

<img src="../../images/week14/2.jpg" width = 900px><br>
<img src="../../images/week14/3.jpg" width = 900px><br>
<img src="../../images/week14/4.jpg" width = 900px><br>
<img src="../../images/week14/5.jpg" width = 900px><br>
<img src="../../images/week14/6.jpg" width = 900px><br>






<iframe width="2026" height="730" src="https://www.youtube.com/embed/nOQkUkDD4p4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



## Codes

Code Board 1

```
#include <SoftwareSerial.h>
SoftwareSerial mySerial(4, 5); // RX, TX



int ledPinrot=0;
int ledPingruen=1;
int trigger=7;                               
int echo=8;                                  
long dauer=0;                                                                        
long entfernung=0;                         

void setup()
{

    mySerial.begin(9600);
    mySerial.println("Board 1");
    pinMode(trigger, OUTPUT);               
    pinMode(echo, INPUT);

}

void loop()
{

    digitalWrite(trigger, LOW);              

    delay(5);                             
    digitalWrite(trigger, HIGH);             

    delay(10);                               
    digitalWrite(trigger, LOW);              

    dauer = pulseIn(echo, HIGH);             

    entfernung = (dauer/2) / 29.1;           


if (entfernung >= 500 || entfernung <= 0)
{
    mySerial.println("can´t read");
    digitalWrite(ledPinrot, HIGH);   
 delay(1000);
}
else
{
    mySerial.print(entfernung);
    mySerial.println(" cm  Board 1");
    digitalWrite(ledPinrot, LOW);
    digitalWrite(ledPingruen, HIGH);



}
delay(1000);
}
```

Code board 2
```

#include <SoftwareSerial.h>
SoftwareSerial mySerial(4, 5); // RX, TX



int ledPinrot=0;
int ledPingruen=1;
int trigger=7;                               
int echo=8;                                  
long dauer=0;                                                                        
long entfernung=0;                         

void setup()
{

    mySerial.begin(9600);
    mySerial.println("Board 1");
    pinMode(trigger, OUTPUT);               
    pinMode(echo, INPUT);

}

void loop()
{

    digitalWrite(trigger, LOW);              

    delay(5);                             
    digitalWrite(trigger, HIGH);             

    delay(10);                               
    digitalWrite(trigger, LOW);              

    dauer = pulseIn(echo, HIGH);             

    entfernung = (dauer/2) / 29.1;           


if (entfernung >= 500 || entfernung <= 0)
{
    mySerial.println("can´t read");
    digitalWrite(ledPinrot, HIGH);   
 delay(1000);
}
else
{
    mySerial.print(entfernung);
    mySerial.println(" cm  Board 1");
    digitalWrite(ledPinrot, LOW);
    digitalWrite(ledPingruen, HIGH);



}
delay(1000);
}

```
