EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "test"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:Microcontroller_ATtiny1614-SSFR IC1
U 1 1 6043C61A
P 5700 3650
F 0 "IC1" H 5700 4531 50  0000 C CNN
F 1 "ATtiny1614-SSFR" H 5700 4440 50  0000 C CNN
F 2 "Fab:SOIC-14_3.9x8.7mm_P1.27mm" H 5700 3650 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf" H 5700 3650 50  0001 C CNN
	1    5700 3650
	1    0    0    -1  
$EndComp
$Comp
L fab:C C1
U 1 1 6043D831
P 6450 2650
F 0 "C1" V 6702 2650 50  0000 C CNN
F 1 "1uF" V 6611 2650 50  0000 C CNN
F 2 "Fab:C_1206" H 6488 2500 50  0001 C CNN
F 3 "" H 6450 2650 50  0001 C CNN
	1    6450 2650
	0    -1   -1   0   
$EndComp
$Comp
L fab:R R1
U 1 1 6043E74A
P 4500 2800
F 0 "R1" V 4707 2800 50  0000 C CNN
F 1 "500" V 4616 2800 50  0000 C CNN
F 2 "Fab:R_1206" V 4430 2800 50  0001 C CNN
F 3 "~" H 4500 2800 50  0001 C CNN
	1    4500 2800
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 6043F29A
P 5700 2650
F 0 "#PWR01" H 5700 2500 50  0001 C CNN
F 1 "VCC" H 5715 2823 50  0000 C CNN
F 2 "" H 5700 2650 50  0001 C CNN
F 3 "" H 5700 2650 50  0001 C CNN
	1    5700 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 60440592
P 5700 4650
F 0 "#PWR02" H 5700 4400 50  0001 C CNN
F 1 "GND" H 5705 4477 50  0000 C CNN
F 2 "" H 5700 4650 50  0001 C CNN
F 3 "" H 5700 4650 50  0001 C CNN
	1    5700 4650
	1    0    0    -1  
$EndComp
$Comp
L fab:BUTTON_B3SN SW1
U 1 1 60441661
P 7550 2950
F 0 "SW1" H 7550 3235 50  0000 C CNN
F 1 "BUTTON_B3SN" H 7550 3144 50  0000 C CNN
F 2 "Fab:Button_Omron_B3SN_6x6mm" H 7550 3150 50  0001 C CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3sn.pdf" H 7550 3150 50  0001 C CNN
	1    7550 2950
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_01x03_Male UPDI1
U 1 1 604447FC
P 6100 4650
F 0 "UPDI1" H 6208 4931 50  0000 C CNN
F 1 "Conn_01x03_Male" H 6208 4840 50  0000 C CNN
F 2 "Fab:fab-1X03" H 6100 4650 50  0001 C CNN
F 3 "~" H 6100 4650 50  0001 C CNN
	1    6100 4650
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_01x06_Male FTDI1
U 1 1 60445387
P 3850 4200
F 0 "FTDI1" H 3958 4581 50  0000 C CNN
F 1 "Conn_01x06_Male" H 3958 4490 50  0000 C CNN
F 2 "Fab:fab-1X06" H 3850 4200 50  0001 C CNN
F 3 "~" H 3850 4200 50  0001 C CNN
	1    3850 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2650 5700 2950
Wire Wire Line
	5700 4350 5700 4650
Wire Wire Line
	6300 4550 6700 4550
Wire Wire Line
	6300 4650 6700 4650
Wire Wire Line
	6300 4750 6700 4750
Text GLabel 6700 4550 2    50   Input ~ 0
UPDI
Text GLabel 6700 4650 2    50   Input ~ 0
GND
Wire Wire Line
	6300 3350 6600 3350
Text GLabel 6600 3350 2    50   Input ~ 0
UPDI
Wire Wire Line
	4050 4000 4450 4000
Wire Wire Line
	4050 4100 4450 4100
Wire Wire Line
	4050 4200 4450 4200
Wire Wire Line
	4050 4300 4450 4300
Wire Wire Line
	4050 4400 4450 4400
Wire Wire Line
	4050 4500 4450 4500
Text GLabel 4450 4000 2    50   Input ~ 0
GND
Text GLabel 4450 4200 2    50   Input ~ 0
VCC
Text GLabel 4450 4300 2    50   Input ~ 0
TX
Text GLabel 4450 4400 2    50   Input ~ 0
RX
NoConn ~ 4450 4100
NoConn ~ 4450 4500
Wire Wire Line
	5100 3550 4800 3550
Wire Wire Line
	5100 3650 4800 3650
Text GLabel 4800 3650 0    50   Input ~ 0
RX
Text GLabel 4800 3550 0    50   Input ~ 0
TX
Text GLabel 6300 2650 0    50   Input ~ 0
VCC
Text GLabel 6600 2650 2    50   Input ~ 0
GND
Wire Wire Line
	4350 2800 4200 2800
Text GLabel 4200 2800 0    50   Input ~ 0
GND
$Comp
L fab:LED LED1
U 1 1 604431F8
P 4900 2800
F 0 "LED1" H 4893 2545 50  0000 C CNN
F 1 "RED" H 4893 2636 50  0000 C CNN
F 2 "Fab:LED_1206" H 4900 2800 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 4900 2800 50  0001 C CNN
	1    4900 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2800 4650 2800
Wire Wire Line
	6300 3450 6500 3450
Text GLabel 6500 3450 2    50   Input ~ 0
BUTTON
Wire Wire Line
	7350 2950 7200 2950
Wire Wire Line
	7750 2950 7950 2950
Text GLabel 7200 2950 0    50   Input ~ 0
BUTTON
Text GLabel 7950 2950 2    50   Input ~ 0
GND
$Comp
L fab:Conn_01x06_Male ultrasonicsensor++1
U 1 1 606D1A6F
P 7350 3500
F 0 "ultrasonicsensor++1" H 7458 3881 50  0000 C CNN
F 1 "Conn_01x06_Male" H 7458 3790 50  0000 C CNN
F 2 "Fab:fab-1X06" H 7350 3500 50  0001 C CNN
F 3 "~" H 7350 3500 50  0001 C CNN
	1    7350 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3400 7950 3400
Text GLabel 7950 3400 2    50   Input ~ 0
VCC
Text GLabel 7950 3700 2    50   Input ~ 0
GND
Wire Wire Line
	7950 3700 7550 3700
Wire Wire Line
	6300 4050 6600 4050
Text GLabel 6600 4050 2    50   Input ~ 0
5
Wire Wire Line
	6300 3950 6600 3950
Text GLabel 6600 3950 2    50   Input ~ 0
4
Wire Wire Line
	6300 3850 6600 3850
Text GLabel 6600 3850 2    50   Input ~ 0
3
Wire Wire Line
	6300 3750 6600 3750
Text GLabel 6600 3750 2    50   Input ~ 0
2
Wire Wire Line
	6300 3650 6600 3650
Text GLabel 6600 3650 2    50   Input ~ 0
13
$Comp
L fab:Conn_01x06_Male vccgnd2-4
U 1 1 606F088C
P 7300 4500
F 0 "vccgnd2-4" H 7408 4881 50  0000 C CNN
F 1 "Conn_01x06_Male" H 7408 4790 50  0000 C CNN
F 2 "Fab:fab-1X06" H 7300 4500 50  0001 C CNN
F 3 "~" H 7300 4500 50  0001 C CNN
	1    7300 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 4300 7900 4300
Wire Wire Line
	7500 4500 7900 4500
Wire Wire Line
	7500 4600 7900 4600
Wire Wire Line
	7500 4700 7900 4700
Text GLabel 7900 4300 2    50   Input ~ 0
GND
Text GLabel 7900 4600 2    50   Input ~ 0
3
Text GLabel 7900 4700 2    50   Input ~ 0
4
Text GLabel 7900 4500 2    50   Input ~ 0
2
Wire Wire Line
	7500 4800 7900 4800
Text GLabel 7900 4800 2    50   Input ~ 0
5
Wire Wire Line
	6300 3550 6600 3550
Text GLabel 6600 3550 2    50   Input ~ 0
12
Wire Wire Line
	5050 2800 5050 3350
Wire Wire Line
	5050 3350 5100 3350
$Comp
L fab:R R2
U 1 1 60716B9F
P 4150 3150
F 0 "R2" V 4357 3150 50  0000 C CNN
F 1 "500" V 4266 3150 50  0000 C CNN
F 2 "Fab:R_1206" V 4080 3150 50  0001 C CNN
F 3 "~" H 4150 3150 50  0001 C CNN
	1    4150 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 3150 3850 3150
Text GLabel 3850 3150 0    50   Input ~ 0
GND
$Comp
L fab:LED LED2
U 1 1 60716BA7
P 4550 3150
F 0 "LED2" H 4543 2895 50  0000 C CNN
F 1 "RED" H 4543 2986 50  0000 C CNN
F 2 "Fab:LED_1206" H 4550 3150 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 4550 3150 50  0001 C CNN
	1    4550 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3150 4300 3150
Wire Wire Line
	5100 3450 4700 3450
Wire Wire Line
	4700 3450 4700 3150
Wire Wire Line
	7550 3500 7950 3500
Text GLabel 7950 3500 2    50   Input ~ 0
12
Wire Wire Line
	7550 3600 7950 3600
Text GLabel 7950 3600 2    50   Input ~ 0
13
Wire Wire Line
	7550 3300 7950 3300
Text GLabel 7950 3300 2    50   Input ~ 0
VCC
Text GLabel 7950 3800 2    50   Input ~ 0
GND
Wire Wire Line
	7950 3800 7550 3800
Text GLabel 6700 4750 2    50   Input ~ 0
VCC
Wire Wire Line
	7500 4400 7900 4400
Text GLabel 7900 4400 2    50   Input ~ 0
VCC
$EndSCHEMATC
