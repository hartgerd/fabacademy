EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "test"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:Microcontroller_ATtiny1614-SSFR IC1
U 1 1 6043C61A
P 5700 3650
F 0 "IC1" H 5700 4531 50  0000 C CNN
F 1 "ATtiny1614-SSFR" H 5700 4440 50  0000 C CNN
F 2 "Fab:SOIC-14_3.9x8.7mm_P1.27mm" H 5700 3650 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf" H 5700 3650 50  0001 C CNN
	1    5700 3650
	1    0    0    -1  
$EndComp
$Comp
L fab:C C1
U 1 1 6043D831
P 5700 2050
F 0 "C1" V 5952 2050 50  0000 C CNN
F 1 "1uF" V 5861 2050 50  0000 C CNN
F 2 "Fab:C_1206" H 5738 1900 50  0001 C CNN
F 3 "" H 5700 2050 50  0001 C CNN
	1    5700 2050
	0    -1   -1   0   
$EndComp
$Comp
L fab:R R1
U 1 1 6043E74A
P 7400 3750
F 0 "R1" V 7607 3750 50  0000 C CNN
F 1 "500" V 7516 3750 50  0000 C CNN
F 2 "Fab:R_1206" V 7330 3750 50  0001 C CNN
F 3 "~" H 7400 3750 50  0001 C CNN
	1    7400 3750
	0    -1   -1   0   
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 6043F29A
P 5700 2650
F 0 "#PWR01" H 5700 2500 50  0001 C CNN
F 1 "VCC" H 5715 2823 50  0000 C CNN
F 2 "" H 5700 2650 50  0001 C CNN
F 3 "" H 5700 2650 50  0001 C CNN
	1    5700 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 60440592
P 5700 4650
F 0 "#PWR02" H 5700 4400 50  0001 C CNN
F 1 "GND" H 5705 4477 50  0000 C CNN
F 2 "" H 5700 4650 50  0001 C CNN
F 3 "" H 5700 4650 50  0001 C CNN
	1    5700 4650
	1    0    0    -1  
$EndComp
$Comp
L fab:BUTTON_B3SN SW1
U 1 1 60441661
P 7550 2950
F 0 "SW1" H 7550 3235 50  0000 C CNN
F 1 "BUTTON_B3SN" H 7550 3144 50  0000 C CNN
F 2 "Fab:Button_Omron_B3SN_6x6mm" H 7550 3150 50  0001 C CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3sn.pdf" H 7550 3150 50  0001 C CNN
	1    7550 2950
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_01x03_Male UPDI1
U 1 1 604447FC
P 6400 2050
F 0 "UPDI1" H 6508 2331 50  0000 C CNN
F 1 "Conn_01x03_Male" H 6508 2240 50  0000 C CNN
F 2 "Fab:fab-1X03" H 6400 2050 50  0001 C CNN
F 3 "~" H 6400 2050 50  0001 C CNN
	1    6400 2050
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_01x06_Male FTDI1
U 1 1 60445387
P 7650 2150
F 0 "FTDI1" H 7758 2531 50  0000 C CNN
F 1 "Conn_01x06_Male" H 7758 2440 50  0000 C CNN
F 2 "Fab:fab-1X06" H 7650 2150 50  0001 C CNN
F 3 "~" H 7650 2150 50  0001 C CNN
	1    7650 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2650 5700 2950
Wire Wire Line
	5700 4350 5700 4650
Wire Wire Line
	6600 1950 7000 1950
Wire Wire Line
	6600 2050 7000 2050
Wire Wire Line
	6600 2150 7000 2150
Text GLabel 7000 1950 2    50   Input ~ 0
UPDI
Text GLabel 7000 2050 2    50   Input ~ 0
GND
Text GLabel 7000 2150 2    50   Input ~ 0
VCC
Wire Wire Line
	6300 3350 6600 3350
Text GLabel 6600 3350 2    50   Input ~ 0
UPDI
Wire Wire Line
	7850 1950 8250 1950
Wire Wire Line
	7850 2050 8250 2050
Wire Wire Line
	7850 2150 8250 2150
Wire Wire Line
	7850 2250 8250 2250
Wire Wire Line
	7850 2350 8250 2350
Wire Wire Line
	7850 2450 8250 2450
Text GLabel 8250 1950 2    50   Input ~ 0
GND
Text GLabel 8250 2150 2    50   Input ~ 0
VCC
Text GLabel 8250 2250 2    50   Input ~ 0
TX
Text GLabel 8250 2350 2    50   Input ~ 0
RX
NoConn ~ 8250 2050
NoConn ~ 8250 2450
Wire Wire Line
	5100 3550 4800 3550
Wire Wire Line
	5100 3650 4800 3650
Text GLabel 4800 3650 0    50   Input ~ 0
RX
Text GLabel 4800 3550 0    50   Input ~ 0
TX
Text GLabel 5550 2050 0    50   Input ~ 0
VCC
Text GLabel 5850 2050 2    50   Input ~ 0
GND
Wire Wire Line
	7550 3750 7700 3750
Text GLabel 7700 3750 2    50   Input ~ 0
GND
$Comp
L fab:LED LED1
U 1 1 604431F8
P 7000 3750
F 0 "LED1" H 6993 3495 50  0000 C CNN
F 1 "RED" H 6993 3586 50  0000 C CNN
F 2 "Fab:LED_1206" H 7000 3750 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 7000 3750 50  0001 C CNN
	1    7000 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	7150 3750 7250 3750
Wire Wire Line
	6300 3750 6850 3750
Wire Wire Line
	6300 3550 6500 3550
Text GLabel 6500 3550 2    50   Input ~ 0
BUTTON
Wire Wire Line
	7350 2950 7200 2950
Wire Wire Line
	7750 2950 7950 2950
Text GLabel 7200 2950 0    50   Input ~ 0
BUTTON
Text GLabel 7950 2950 2    50   Input ~ 0
GND
$EndSCHEMATC
